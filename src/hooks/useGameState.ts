import React from 'react';
import lists from '../list.json';
import { Mode } from '../components/Game.js';

interface GameState {
  solution?: string;
  guesses: string[];
  finale: boolean;
  currentGuess: string;
  foundLetters: Array<string | null>;
  mode: Mode;
};

interface Options {
  timeout?: number;
};
export function useGameState({
  timeout = -1,
}: Options = {}) {
  const [ mode, setMode ] = React.useState<Mode>(6);
  const finaleToggler: React.Reducer<boolean, void> = (isFinale) => !isFinale;
  const [ finale, toggleFinale ] = React.useReducer(finaleToggler, false);
  const [ solution, setSolution ] = React.useState<string>();
  const [ previousSolutions, addSolution ] = React.useReducer(
    (solutions: string[], solution: string) => solutions.concat(solution),
    [],
  )
  const topWordsList: string[] = (lists as any)[mode].top;
  React.useEffect(() => {
    setSolution(topWordsList[Math.floor(Math.random() * topWordsList.length)]);
  }, [topWordsList, previousSolutions]);
  const [ guesses, addGuess ] = React.useReducer(
    (guesses: string[], guess: string | null) => guess === null ? [] : guesses.concat(guess),
    [],
  );
  const givenChars = React.useMemo(
    () => {
      if (!finale || !solution) {
        return [0];
      }

      // We want a random character in `solution`, other than the first one (which is always given):
      const otherChar = Math.floor(Math.random() * Math.floor(solution.length - 1)) + 1;
      return [0, otherChar];
    },
    [finale, solution],
  );

  const addCharToGuess: React.Reducer<string, string | null> = (guess, char) => {
    if (char === null) {
      return '';
    }

    if (char === 'backspace') {
      return guess.substring(0, guess.length - 1);
    }

    if (char === 'j' && guess.substring(guess.length - 1) === 'i') {
      return guess.substring(0, guess.length - 1) + 'ĳ';
    }
    if (solution && guess.length === solution.length) {
      // Do not add characters if we are at the required length
      return guess;
    }
    return guess + char;
  };
  const [ currentGuess, addChar ] = React.useReducer(addCharToGuess, '');

  React.useEffect(() => {
    if (timeout !== -1) {
      const timeoutRef = setTimeout(() => {
        if (guesses[guesses.length - 1] !== solution) {
          addGuess('.'.repeat(mode));
        }
      }, timeout * 1000);
      return () => clearTimeout(timeoutRef);
    }
  }, [guesses, currentGuess, mode, timeout, solution]);

  const foundLetters = guesses.reduce<Array<string | null>>((foundLetters, guess) => {
    if (!solution) {
      return [];
    }
    return solution.split('').map((correctLetter, i) => {
      if (foundLetters[i]) {
        return foundLetters[i];
      }
      if (guess.charAt(i) === correctLetter) {
        return correctLetter;
      }
      return null;
    });
  }, []);
  if (solution) {
    givenChars.forEach(char => foundLetters[char] = solution.charAt(char));
  }

  function loadNext() {
    if (!solution) {
      return;
    }
    addSolution(solution);
    addChar(null);
    addGuess(null);
  }

  const gameState: GameState = {
    currentGuess,
    guesses,
    mode,
    finale,
    solution,
    foundLetters,
  };

  const modifiers = {
    setMode,
    addChar,
    addGuess,
    loadNext,
    toggleFinale,
  };

  return [ gameState, modifiers ] as const;
}
