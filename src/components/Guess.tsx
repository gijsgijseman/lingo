import React from 'react';
import { validate } from '../validate';

interface Props {
  guess: string;
  solution: string;
  validate: boolean;
  foundLetters: Array<string | null>;
};

export const Guess: React.FC<Props> = (props) => {
  const guessChars = props.guess.split('');
  const solutionChars = props.solution.split('');
  const validation = validate(guessChars, solutionChars);

  const cards = solutionChars.map((_char, i) => {
    const validationClass = props.validate ? validation[i] : '';

    return (
      <div key={i} className={`level-item is-size-1 charGuess ${validationClass}`}>
        <span className="letter">
          {guessChars[i] || ((props.foundLetters[i]) ? <span className="hint">{props.foundLetters[i]}</span> : <>.</>)}
        </span>
      </div>
    );
  });

  return (
    <div className="level is-mobile guess">
      {cards}
    </div>
  );
};
